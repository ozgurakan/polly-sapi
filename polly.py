# SAPI5 Compliant Speech Engine For Polly
from winerror import S_OK


class AWSPollyEngine:
    """
    Implements Polly intergration
    """
    IID_ISpTTSEngine = '{A74D7C8E-4CC5-4f2f-A6EB-804DEE18500E}'
    IID_ISpObjectToken = '{14056589-E16C-11D2-BB90-00C04F8EE6C0}'
    IID_ISpTTSEngineSSML = '{2D0FA0DB-AEA2-4AE2-9F8A-7AFC7794E56B}'
    IID_ISpVoice = '{6C44DF74-72B9-4992-A1EC-EF996E0422D4}'
    IID_ISpObjectWithToken = '{5B559F40-E952-11D2-BB91-00C04F8EE6C0}'

    _public_methods_ = ['test', 'Speak'
                        'SetObjectToken', 'GetObjectToken',
                        'GetOutputFormat']
    _reg_progid_ = "Polly.Engine"
    _reg_clsid_ = "{9166A528-B0A9-43DB-9340-01B7A0E24E2A}"

    _com_interfaces_ = [IID_ISpTTSEngine,
                        IID_ISpObjectWithToken,
                        IID_ISpTTSEngineSSML]

    def __init__(self):
        with open('D:\\Users\\ozakan\\dev\\tts\\output\\polly.log', 'a') as log_file:
            log_file.write('I am running\n')

    def test(self, value=None):
        """
        Test method
        """
        if value:
            return True
        return False

    def SetObjectToken(self, token):
        '''Set token'''
        with open('D:\\Users\\ozakan\\dev\\tts\\output\\speak.log', 'a') as log_file:
            log_file.write('SetObjectToken method - token: {}\n'.format(token))
        return token

    def GetObjectToken(self, token):
        '''Get Token'''
        with open('D:\\Users\\ozakan\\dev\\tts\\output\\speak.log', 'a') as log_file:
            log_file.write('GetObjectToken method\n')
        return token

    def Speak1(self,
              dwSpeakFlags=0,
              rguidFormatId='SPDFID_Text',
              pWaveFormatEx=None,
              pTextFragList='Hello World',
              pOutputSite=0):
        """
        speak implementation
        """
        with open('D:\\Users\\ozakan\\dev\\tts\\output\\speak.log', 'a') as log_file:
            log_file.write('Speak method: {}\n'.format(pTextFragList))
        print('here I will say: {}'.format(pTextFragList))

        return pWaveFormatEx, pTextFragList, pOutputSite

    def Speak(self, *args):
        """
        speak implementation
        """
        if args is not None:
            print('Listing arguments: {}'.format(args))
            for arg in args:
                print('{}-{}'.format(len(args), arg))
        else:
            print('No arguments passed to Speak')
        with open('D:\\Users\\ozakan\\dev\\tts\\output\\speak.log', 'a') as log_file:
            log_file.write('Speak method\n')
        print('here I will say: {}'.format(args))

        return S_OK


    def GetOutputFormat(self, pTargetFormatId, pTargetWaveFormatEx,
                        pDesiredFormatId, ppCoMemDesiredWaveFormatEx):
        with open('D:\\Users\\ozakan\\dev\\tts\\output\\speak.log', 'a') as log_file:
            log_file.write('GetOutputFormat method\n')

        return S_OK

if __name__ == '__main__':
    print("Registering COM Server")
    import win32com.server.register
    win32com.server.register.UseCommandLine(AWSPollyEngine, debug=0)
    