# Polly SAPI Engine Implementation

- polly.py: implements Speech API. Uses win32com and registers a COM server
- talk.py: loops over available voices, since polly is one of them, invokes polly voice
- error.txt: error message displayed when talk.py is run

## Notes

- MS registry has to be updated in order to make Polly voice available. Example [here][1]

## References

- [Microsoft Speech API][2]
- [Vendor Porting Guide][3]
- [TTS Engine Interface][4]

[1]:	https://msdn.microsoft.com/en-us/library/ee431802(v=vs.85).aspx#Creating%20and%20Initializing%20the%20Engine%20-%20ISpObjectWithToken
[2]:	https://msdn.microsoft.com/en-us/library/ee125663(v=vs.85).aspx
[3]:	https://msdn.microsoft.com/en-us/library/ee431802(v=vs.85).aspx
[4]:	https://msdn.microsoft.com/en-us/library/ee431824(v=vs.85).aspx