import sys
import win32com.client
import win32api
import pythoncom
from winreg import *
from pythoncom import com_error

def speak_test():
    speaker = win32com.client.Dispatch("SAPI.SpVoice")
    print('Default voice: {}'.format(speaker.Voice.GetDescription()))
    say_this = "Hello"

    tokens = speaker.GetVoices()
    print('count {}'.format(tokens.Count))
    for i in range(tokens.Count):
        token = tokens.Item(i)
        print('Voice token: {}'.format(str(token.GetDescription())))
        speaker.SetVoice(token)
        print('Current voice: {}'.format(speaker.Voice.GetDescription()))        
        try:
            value = speaker.Speak(say_this)
            print('speak return value: {}'.format(value))
        except com_error as myerror:
            print(win32api.FormatMessage(myerror.excepinfo[5]))
            print(vars(myerror))
            raise

def print_keys(key, sub_key=None):
    if sub_key:
        key = OpenKey(key, sub_key)

    print('{} Values\n--------'.format(sub_key))
    for i in range(0, 1024):
        try:
            print(EnumValue(key, i))
        except EnvironmentError:
            break

    print('{} Sub Keys\n-------'.format(sub_key))
    for i in range(0, 1024):
        try:
            sub_key = EnumKey(key, i)
            print(sub_key)
            print_keys(key, sub_key)
        except EnvironmentError:
            break

def helper_test():
    aReg = ConnectRegistry(None, HKEY_LOCAL_MACHINE)
    aKey = OpenKey(aReg, r"SOFTWARE\Microsoft\Speech\Voices\Tokens")
    print_keys(aKey, 'TTS_MS_EN-US_DAVID_11.0')
    #print_keys(aKey)
    CloseKey(aKey) 


if __name__ == "__main__":
    from win32com.client import gencache
    gencache.EnsureModule('{5F7209DB-16C5-45AE-A67D-60B8585100CA}', 0, 10, 0)
    gencache.EnsureModule('{EB2114C0-CB02-467A-AE4D-2ED171F05E6A}', 0, 10, 0)
    #gencache.EnsureModule('{6C44DF74-72B9-4992-A1EC-EF996E0422D4}', 0, 10, 0)
    speak_test()

